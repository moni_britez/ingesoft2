/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpuna;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RegisterServlet", urlPatterns = "/register")
public class RegisterView extends HttpServlet{
    
    private final String NAME_USERID = "inge_userid";
    private final String NAME_USERDATA = "inge_userdata";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/register/register.jsp");
        Cookie cookies[] = req.getCookies();
        /* SI esta autenticado no puede acceder a esta pagina */
        if(cookies != null){
            for (Cookie actual : cookies) {
                String nameCookie = actual.getName();
                if(NAME_USERID.equals(nameCookie) || NAME_USERDATA.equals(nameCookie)){
                    // Si esta autenticado se va a ir al home
                    if(actual.getValue() != null){ dispatcher = req.getRequestDispatcher("/WEB-INF/home/home.jsp"); }
                    break;
                }
            }
        }
        dispatcher.forward(req, resp);
    }
    
}
