/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpuna;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "HomeServlet", urlPatterns = "/home")
public class HomeView extends HttpServlet{

    private final String NAME_USERID = "inge_userid";
    private final String NAME_USERDATA = "inge_userdata";
    private final String NAME_USERROL = "inge_userrol";
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/home/home.jsp");
        Cookie cookies[] = req.getCookies(); Boolean authentication = false;
        /* SI no esta autenticado no puede acceder a esta pagina */
        if(cookies != null){
            for (Cookie actual : cookies) {
                String nameCookie = actual.getName();
                if(NAME_USERID.equals(nameCookie) || NAME_USERDATA.equals(nameCookie)){
                    if(actual.getValue() != null){ authentication = true; }
                }
                if(NAME_USERROL.equals(nameCookie)){ req.setAttribute("idRol", actual.getValue()); }
                if(NAME_USERDATA.equals(nameCookie)){ req.setAttribute("userData", actual.getValue()); }
            }
        }
        if(!authentication){ dispatcher = req.getRequestDispatcher("/WEB-INF/login/login.jsp"); }
        dispatcher.forward(req, resp);
    }
    
}
