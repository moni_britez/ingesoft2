<%-- 
    Document   : login
    Created on : 25/06/2019, 10:41:07 PM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="resources/bootstrap.min.css">
        <script src="resources/bootstrap.min.js"></script>
        <script><%@include file="/WEB-INF/login/login.js"%></script>
        <script><%@include file="/WEB-INF/core/cookie.js"%></script>
        <script><%@include file="/WEB-INF/core/snackbar.js"%></script>
        <script><%@include file="/WEB-INF/core/optionApp.js"%></script>
    </head>
    <body style="background: #eeeeee" id="currentBody">
        <div class="container-fluid" style="margin-top: 50px">
            <div class="col-sm-6 col-xs-12" style="margin: auto; margin-top: 30px">
                <div class="card">
                    <div class="card-header" style="background: black; color: white; font-weight: bold">
                        Login
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" id="inputUsername" placeholder="Nombre de usuario">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="inputPassword" placeholder="Contraseña">
                            </div>
                        </form>
                        <button type="submit" class="btn btn-primary" onclick="login()">Iniciar sesión</button>
                        <button class="btn btn-secondary" onclick="goToRegister()">Registrarse</button>                        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
