/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function goToRegister(){ window.location.href = "register"; }
function login() {
    var xhr = new XMLHttpRequest();
    var username = document.getElementById("inputUsername").value;
    var password = document.getElementById("inputPassword").value;
    var loginRequest = {usuario: username, password: password};
    xhr.open("POST", API_URL + "login", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
            var response = JSON.parse(xhr.responseText);
            createCookie("inge_userid", response.id_usuario, 15);
            createCookie("inge_userdata", JSON.stringify(response), 15);
            createCookie("inge_userrol", response.rol.id, 15);
            window.location.href = "home";
        }
        else {
            var response = xhr.responseText;
            show("Credenciales invalidas", "error");
        }
    };
    xhr.send(JSON.stringify(loginRequest));
}