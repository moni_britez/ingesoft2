<%-- 
    Document   : login
    Created on : 25/06/2019, 10:41:07 PM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String idRol = (String) request.getAttribute("idRol");
    Boolean admin = idRol.equals("2") ? true : false;
%>
<!DOCTYPE html>
<html class="h-100">
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="resources/bootstrap.min.css">
        <script src="resources/bootstrap.min.js"></script>
        <script src="resources/jquery.js"></script>
        <script><%@include file="/WEB-INF/home/home.js"%></script>
        <script><%@include file="/WEB-INF/core/cookie.js"%></script>
        <script><%@include file="/WEB-INF/core/snackbar.js"%></script>        
        <script><%@include file="/WEB-INF/core/optionApp.js"%></script> 
        <style><%@include file="/WEB-INF/home/home.css"%></style>
    </head> 
    <body style="background: #FAFAFA" class="h-100" onload="loadDataUsername()" id="currentBody">
        <div class="container-fluid h-100" style="padding: 0; margin: 0; display: flex">
            <div class="col-2 h-100" style="padding: 0; margin: 0">
                <div class="list-group h-100" id="list-tab" role="tablist" style="background: white">
                    <a class="list-group-item list-group-item-action text-center" style="background: #004481; color: white">
                        <span class="font-weight-bold">GESTION DE PROYECTOS</span>
                    </a>
                    <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" onclick="loadProjectView()">Proyectos</a>
                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile" onclick="loadSprintView()">Sprint</a>
                    <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages" onclick="loadBacklogView()">Backlog</a>
                    <%if(admin){%>
                        <a class="list-group-item list-group-item-action" id="list-users-list" data-toggle="list" href="#list-users" role="tab" aria-controls="users" onclick="loadUsersView()">Usuarios</a>
                    <%}%>
                    <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings" onclick="loadUserData()">Acerca de</a>
                    <div id="userTitleData" style="margin-top: auto; text-align: center"></div>
                    <button type="button" class="btn btn-outline-danger" style="margin: 10px" onclick="logout()">Cerrar sesión</button>
                </div>
            </div>
            <div class="col-10" style="padding: 15px;">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                        <%@include file="/WEB-INF/project/project.jsp"%>
                    </div>
                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                        <%@include file="/WEB-INF/sprint/sprint.jsp"%>
                    </div>
                    <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                        <%@include file="/WEB-INF/backlog/backlog.jsp"%>
                    </div>
                    <div class="tab-pane fade" id="list-users" role="tabpanel" aria-labelledby="list-users-list">
                        <%@include file="/WEB-INF/users/users.jsp"%>
                    </div>                    
                    <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                        <%@include file="/WEB-INF/configuration/configuration.jsp"%>
                    </div>
                </div>
            </div>
        </div>
        <script src="resources/bootstrap.min.js"></script>
        <script src="resources/jquery.js"></script>        
    </body>
</html>
