/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function logout(){
    eraseCookie("inge_userid");
    eraseCookie("inge_userdata");
    eraseCookie("inge_userrol");
    window.location.href = "login";
}

function checkAccess(){
    var userRol = readCookie("inge_userrol");
    // Si es admin
    return userRol == 2;
}

function loadDataUsername(){
    var div = document.getElementById("userTitleData");
    removeAllChilds(div);
    var data = readCookie("inge_userdata");
    if(data === null || data === undefined){
        window.location.href = "login";
    }
    else{
        data = JSON.parse(data);
        var nombreH6 = document.createElement("h6");
        nombreH6.innerHTML = "<strong>BIENVENIDO " + data.nombre.toUpperCase() + " " + data.apellido.toUpperCase(); + "</strong>";
        div.appendChild(nombreH6);
    }
    loadProjectView();
}