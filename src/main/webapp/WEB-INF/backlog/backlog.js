function loadBacklogView(){
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        // Si es admin
        if (userData.id_rol == 2) {
            loadUserBacklogs(userData.id_usuario);
            loadAllBacklogs();
        } else {
            loadUserBacklogs(userData.id_usuario);
        }
    }
}

function loadUserBacklogs(idUsuario) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/product-backlog/users/" + idUsuario, false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadBacklogsInDOM(response, "misBacklogsTable");
            } else {
                var div = document.getElementById("misBacklogsTable"); removeAllChilds(div);
                var messageH6 = document.createElement("h6");
                messageH6.innerText = "No hay nada para mostrar";
                div.appendChild(messageH6);
            }
        } else {
            var div = document.getElementById("misBacklogsTable"); removeAllChilds(div);
            var messageH6 = document.createElement("h6");
            messageH6.innerText = "No hay nada para mostrar";
            div.appendChild(messageH6);
        }
    };
    xhr.send();
}

function loadAllBacklogs() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/product-backlog/all", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadBacklogsInDOM(response, "allBacklogsTable");
            } else {
                var div = document.getElementById("allBacklogsTable"); removeAllChilds(div);
                var messageH6 = document.createElement("h6");
                messageH6.innerText = "No hay nada para mostrar";
                div.appendChild(messageH6);
            }
        } else {
            var div = document.getElementById("allBacklogsTable"); removeAllChilds(div);
            var messageH6 = document.createElement("h6");
            messageH6.innerText = "No hay nada para mostrar";
            div.appendChild(messageH6);
        }
    };
    xhr.send();
}

function loadBacklogsInDOM(projects, divTable) {
    
    var div = document.getElementById(divTable); removeAllChilds(div);
    var table = document.createElement("table");
    
    // creacion de la tabla
    table.setAttribute("class", "table table-hover");
    table.setAttribute("style", "margin-top: 20px");
    
    // cabeceras
    
    var thead = document.createElement("thead");
    var trhead = document.createElement("tr");
    
    var thIdHead = document.createElement("th"); thIdHead.innerText = "#";
    thIdHead.setAttribute("scope", "col"); trhead.appendChild(thIdHead);
    
    var thDescriptionHead = document.createElement("th"); thDescriptionHead.innerText = "Descripcion";
    thDescriptionHead.setAttribute("scope", "col"); trhead.appendChild(thDescriptionHead);
    
    var thPrioridadHead = document.createElement("th"); thPrioridadHead.innerText = "Prioridad";
    thPrioridadHead.setAttribute("scope", "col"); trhead.appendChild(thPrioridadHead);
    
    var thDuracionHead = document.createElement("th"); thDuracionHead.innerText = "Duracion";
    thDuracionHead.setAttribute("scope", "col"); trhead.appendChild(thDuracionHead);

    var thSprintHead = document.createElement("th"); thSprintHead.innerText = "Sprint";
    thSprintHead.setAttribute("scope", "col"); trhead.appendChild(thSprintHead);
    
    var thCreadorHead = document.createElement("th"); thCreadorHead.innerText = "Creador";
    thCreadorHead.setAttribute("scope", "col"); trhead.appendChild(thCreadorHead);
    
    var tEncargadoHead = document.createElement("th"); tEncargadoHead.innerText = "Encargado";
    tEncargadoHead.setAttribute("scope", "col"); trhead.appendChild(tEncargadoHead);    
    
    var thActionsHead = document.createElement("th"); thActionsHead.innerText = "Acciones";
    thActionsHead.setAttribute("scope", "col"); trhead.appendChild(thActionsHead);
    
    thead.appendChild(trhead);
    
    var tbody = document.createElement("tbody");
            
    for(var i = 0; i < projects.length; i++){
        
        var tr = document.createElement("tr");
        
        var tdId = document.createElement("td");
        tdId.innerText = projects[i].idProduct; tr.appendChild(tdId);
        
        var tdDescripcion = document.createElement("td");
        tdDescripcion.innerText = projects[i].descripcion; tr.appendChild(tdDescripcion);
        
        var tdPrioridad = document.createElement("td");
        tdPrioridad.innerText = projects[i].prioridad; tr.appendChild(tdPrioridad);
        
        var tdDuracion = document.createElement("td");
        tdDuracion.innerText = projects[i].duracion; tr.appendChild(tdDuracion);                        
        
        var tdSprint = document.createElement("td");
        tdSprint.innerText = projects[i].idSprint.nombre; tr.appendChild(tdSprint);                
        
        var tdCreador = document.createElement("td");
        tdCreador.innerText = projects[i].idUsuario.nombre + " " + projects[i].idUsuario.apellido;
        tr.appendChild(tdCreador);
        
        var tdEncargado = document.createElement("td");
        tdEncargado.innerText = projects[i].encargado.nombre + " " + projects[i].encargado.apellido;
        tr.appendChild(tdEncargado);
        
        var tdAcciones = document.createElement("td");
        var btnEditar = document.createElement("button"); btnEditar.innerText = "Editar";
        btnEditar.setAttribute("class", "btn btn-primary");
        btnEditar.setAttribute("style", "margin-right: 5px");
        btnEditar.setAttribute("data-toggle", "modal");
        btnEditar.setAttribute("data-target", "#formEditBacklog");
        btnEditar.setAttribute("onclick", "handleShowBacklogEditData(" + JSON.stringify(projects[i]) +  ")");        
        
        var btnEliminar = document.createElement("button"); btnEliminar.innerText = "Eliminar";
        btnEliminar.setAttribute("class", "btn btn-secondary");        
        btnEliminar.setAttribute("onclick", "handleDeleteBacklog(" + projects[i].idProduct + ")");        
        tdAcciones.appendChild(btnEditar); tdAcciones.appendChild(btnEliminar);
        
        tr.appendChild(tdAcciones);
                
        tbody.appendChild(tr);
    }
    
    table.appendChild(thead);
    table.appendChild(tbody);
    div.appendChild(table);
}

function handleSaveBacklog() {
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        saveBacklog(userData);
    }
}

function saveBacklog(userData) {
    var xhr = new XMLHttpRequest();
    var descripcion = document.getElementById("backlogInputDescripcion").value;
    var duracion = document.getElementById("backlogInputDuracion").value;
    var prioridad = document.getElementById("backlogInputPrioridad").value;
    var idSprint = document.getElementById("selectBacklogInputSprint").value;
    var idUsuario = userData.id_usuario;
    var idEncargado = document.getElementById("selectBacklogInputEncargado").value;
    var backlogPost = {
        descripcion: descripcion,
        duracion: duracion,
        prioridad: prioridad,
        idSprint: idSprint,
        idUsuario: idUsuario,
        encargado: idEncargado
    };
    xhr.open("POST", API_URL + "v1/product-backlog/save", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
            var response = JSON.parse(xhr.responseText);
            show("Se ha registrado exitosamente", "success");
            loadBacklogView();
        } else {
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");
        }
    };
    xhr.send(JSON.stringify(backlogPost));
}

function handleShowBacklogEditData(backlog){
    document.getElementById("edit_backlogInputId").value = backlog.idProduct;
    document.getElementById("edit_backlogInputDescripcion").value = backlog.descripcion;    
    document.getElementById("edit_backlogInputDuracion").value = backlog.duracion;    
    document.getElementById("edit_backlogInputPrioridad").value = backlog.prioridad;   
    loadSprintEditInSelect(backlog.idSprint.idSprint);
    loadEncargadoEditInSelect(backlog.encargado.id_usuario);   
}

function loadSprintEditInSelect(idProyecto) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/sprint/all", false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadSprintEditInSelectDOM(response, idProyecto);
            } else {
                var div = document.getElementById("edit_backlogInputSprint");
                removeAllChilds(div);
                var select = document.createElement("select");
                select.setAttribute("class", "form-control");
                var option = document.createElement("option");
                option.setAttribute("value", "-1");
                option.innerText = "No hay sprints";
                select.appendChild(option);
                div.appendChild(select);
            }
        } else {
            var div = document.getElementById("edit_backlogInputSprint");
            removeAllChilds(div);
            var select = document.createElement("select");
            select.setAttribute("class", "form-control");
            select.setAttribute("id", "editselect_backlogInputSprint");
            var option = document.createElement("option");
            option.setAttribute("value", "-1");
            option.innerText = "No hay sprints";
            select.appendChild(option);
            div.appendChild(select);
        }
    };
    xhr.send();
}

function loadSprintEditInSelectDOM(projects, idProyecto) {
    var div = document.getElementById("edit_backlogInputSprint");
    removeAllChilds(div);
    var select = document.createElement("select");
    select.setAttribute("class", "form-control");
    select.setAttribute("id", "editselect_backlogInputSprint");
    for (var i = 0; i < projects.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", projects[i].idSprint);
        option.innerText = projects[i].nombre;
        if(projects[i].idSprint == idProyecto){
            option.setAttribute("selected", "true");
        }
        select.appendChild(option);
    }
    div.appendChild(select);
}

function loadEncargadoEditInSelect(idProyecto) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/user/all", false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadEncargadoEditInSelectDOM(response, idProyecto);
            } else {
                var div = document.getElementById("edit_backlogInputEncargado");
                removeAllChilds(div);
                var select = document.createElement("select");
                select.setAttribute("class", "form-control");
                var option = document.createElement("option");
                option.setAttribute("value", "-1");
                option.innerText = "No hay usuarios";
                select.appendChild(option);
                div.appendChild(select);
            }
        } else {
            var div = document.getElementById("edit_backlogInputEncargado");
            removeAllChilds(div);
            var select = document.createElement("select");
            select.setAttribute("class", "form-control");
            select.setAttribute("id", "editselect_backlogInputEncargado");
            var option = document.createElement("option");
            option.setAttribute("value", "-1");
            option.innerText = "No hay usuarios";
            select.appendChild(option);
            div.appendChild(select);
        }
    };
    xhr.send();
}

function loadEncargadoEditInSelectDOM(projects, idProyecto) {
    var div = document.getElementById("edit_backlogInputEncargado");
    removeAllChilds(div);
    var select = document.createElement("select");
    select.setAttribute("class", "form-control");
    select.setAttribute("id", "editselect_backlogInputEncargado");
    for (var i = 0; i < projects.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", projects[i].id_usuario);
        option.innerText = projects[i].nombre + " " + projects[i].apellido;
        if(projects[i].id_usuario == idProyecto){
            option.setAttribute("selected", "true");
        }
        select.appendChild(option);
    }
    div.appendChild(select);
}

function handleEditBacklog() {
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        editBacklog(userData);
    }
}

function editBacklog(userData) {
    var xhr = new XMLHttpRequest();
    var idBacklog = document.getElementById("edit_backlogInputId").value;
    var descripcion = document.getElementById("edit_backlogInputDescripcion").value;
    var duracion = document.getElementById("edit_backlogInputDuracion").value;
    var prioridad = document.getElementById("edit_backlogInputPrioridad").value;
    var idSprint = document.getElementById("editselect_backlogInputSprint").value;
    var idEncargado = document.getElementById("editselect_backlogInputEncargado").value;
    var idUsuario = userData.id_usuario;    
    var backlogPost = {
        idProduct: idBacklog,
        descripcion: descripcion,
        duracion: duracion,
        prioridad: prioridad,
        idSprint: idSprint,
        idUsuario: idUsuario,
        encargado: idEncargado
    };
    xhr.open("PUT", API_URL + "v1/product-backlog/update", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            show("Se ha actualizado exitosamente", "success");
            loadBacklogView();
        } else {
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");
        }
    };
    xhr.send(JSON.stringify(backlogPost));
}

function handleDeleteBacklog(idProject){
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        deleteBacklog(idProject);
    }    
}

function deleteBacklog(idProject){
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", API_URL + "v1/product-backlog/delete/" + idProject, false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
            show("Se ha eliminado exitosamente", "success");
            loadBacklogView();
        }
        else{
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");            
        }
    };
    xhr.send();
}