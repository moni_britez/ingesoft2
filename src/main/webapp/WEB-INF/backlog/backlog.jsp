<%-- 
    Document   : login
    Created on : 25/06/2019, 10:41:07 PM
    Author     : gino_junchaya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proyectos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="resources/bootstrap.min.css">
        <script src="resources/bootstrap.min.js"></script>
        <script><%@include file="/WEB-INF/backlog/backlog.js"%></script>
    </head>
    <body>
        <div>
            <h3>Backlog</h3>
            <div style="margin-top: 20px">
                <h5>Mis tareas</h5>
                <button class="btn btn-primary" style="margin-top: 10px; margin-bottom: 10px" data-toggle="modal" data-target="#formBacklog" onclick="loadSelectsBacklog()">Nuevo</button>                
                <div id="misBacklogsTable" class="table-responsive"></div>
            </div>
            <%if (admin) {%>
            <div style="margin-top: 20px">
                <h5>Todas las tareas</h5>
                <div id="allBacklogsTable" class="table-responsive"></div>
            </div>
            <%}%>
            <%@include file="/WEB-INF/modal/formBacklog.jsp"%>
            <%@include file="/WEB-INF/modal/formEditBacklog.jsp"%>            
        </div>
    </body>
</html>
