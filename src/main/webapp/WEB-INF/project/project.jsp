<%-- 
    Document   : login
    Created on : 25/06/2019, 10:41:07 PM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proyectos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="resources/bootstrap.min.css">
        <script src="resources/bootstrap.min.js"></script>
        <script><%@include file="/WEB-INF/project/project.js"%></script>
    </head>
    <body>
        <div>
            <h3>Proyectos</h3>
            <div id="misProyectos" style="margin-top: 20px">
                <h5>Mis proyectos</h5>
                <button class="btn btn-primary" style="margin-top: 10px; margin-bottom: 10px" data-toggle="modal" data-target="#formProject">Nuevo</button>                
                <div id="misProyectosTable" class="table-responsive"></div>
            </div>
            <%if (admin) {%>
            <div id="allProjects" style="margin-top: 20px">
                <h5>Todos los proyectos</h5>
                <div id="allProyectosTable" class="table-responsive"></div>
            </div>
            <%}%>
            <%@include file="/WEB-INF/modal/formProject.jsp"%>
            <%@include file="/WEB-INF/modal/formEditProject.jsp"%>            
        </div>
    </body>
</html>
