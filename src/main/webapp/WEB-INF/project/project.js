/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function loadProjectView() {
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        // Si es admin
        if (userData.id_rol == 2) {
            loadUserProjects(userData.id_usuario);
            loadAllProjects();
        } else {
            loadUserProjects(userData.id_usuario);
        }
    }
}

function loadUserProjects(idUsuario) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/project/" + idUsuario, false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadProjectsInDOM(response, "misProyectosTable");
            } else {
                var div = document.getElementById("misProyectosTable"); removeAllChilds(div);
                var messageH6 = document.createElement("h6");
                messageH6.innerText = "No hay nada para mostrar";
                div.appendChild(messageH6);
            }
        } else {
            var div = document.getElementById("misProyectosTable"); removeAllChilds(div);
            var messageH6 = document.createElement("h6");
            messageH6.innerText = "No hay nada para mostrar";
            div.appendChild(messageH6);
        }
    };
    xhr.send();
}

function loadAllProjects() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/project/all", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadProjectsInDOM(response, "allProyectosTable");
            } else {
                var div = document.getElementById("allProyectosTable"); removeAllChilds(div);
                var messageH6 = document.createElement("h6");
                messageH6.innerText = "No hay nada para mostrar";
                div.appendChild(messageH6);
            }
        } else {
            var div = document.getElementById("allProyectosTable"); removeAllChilds(div);
            var messageH6 = document.createElement("h6");
            messageH6.innerText = "No hay nada para mostrar";
            div.appendChild(messageH6);
        }
    };
    xhr.send();
}

function loadProjectsInDOM(projects, divTable) {
    
    var div = document.getElementById(divTable); removeAllChilds(div);
    var table = document.createElement("table");
    table.setAttribute("id", "dtBasicExample");
    
    // creacion de la tabla
    table.setAttribute("class", "table table-hover");
    table.setAttribute("style", "margin-top: 20px");
    
    // cabeceras
    
    var thead = document.createElement("thead");
    var trhead = document.createElement("tr");
    
    var thIdHead = document.createElement("th"); thIdHead.innerText = "#";
    thIdHead.setAttribute("scope", "col"); trhead.appendChild(thIdHead);
    
    var thDescriptionHead = document.createElement("th"); thDescriptionHead.innerText = "Descripcion";
    thDescriptionHead.setAttribute("scope", "col"); trhead.appendChild(thDescriptionHead);
    
    var thEstadoHead = document.createElement("th"); thEstadoHead.innerText = "Estado";
    thEstadoHead.setAttribute("scope", "col"); trhead.appendChild(thEstadoHead);
    
    var thCreadorHead = document.createElement("th"); thCreadorHead.innerText = "Creador";
    thCreadorHead.setAttribute("scope", "col"); trhead.appendChild(thCreadorHead);
    
    var thInicioHead = document.createElement("th"); thInicioHead.innerText = "Inicio";
    thInicioHead.setAttribute("scope", "col"); trhead.appendChild(thInicioHead);
    
    var thFinHead = document.createElement("th"); thFinHead.innerText = "Fin";
    thFinHead.setAttribute("scope", "col"); trhead.appendChild(thFinHead);
    
    var thActionsHead = document.createElement("th"); thActionsHead.innerText = "Acciones";
    thActionsHead.setAttribute("scope", "col"); trhead.appendChild(thActionsHead);
    
    thead.appendChild(trhead);
    
    var tbody = document.createElement("tbody");
            
    for(var i = 0; i < projects.length; i++){
        
        var tr = document.createElement("tr");
        
        var tdId = document.createElement("td");
        tdId.innerText = projects[i].id_proyecto; tr.appendChild(tdId);
        
        var tdDescripcion = document.createElement("td");
        tdDescripcion.innerText = projects[i].descripcion; tr.appendChild(tdDescripcion);
        
        var tdEstado = document.createElement("td");
        tdEstado.innerText = projects[i].estado; tr.appendChild(tdEstado);
        
        var tdCreador = document.createElement("td");
        tdCreador.innerText = projects[i].creador.nombre + " " + projects[i].creador.apellido;
        tr.appendChild(tdCreador);
        
        var tdInicio = document.createElement("td");
        tdInicio.innerText = projects[i].fecha_inicio; tr.appendChild(tdInicio);
        
        var tdFin = document.createElement("td");
        tdFin.innerText = projects[i].fecha_fin; tr.appendChild(tdFin);
        
        var tdAcciones = document.createElement("td");
        var btnEditar = document.createElement("button"); btnEditar.innerText = "Editar";
        btnEditar.setAttribute("class", "btn btn-primary");
        btnEditar.setAttribute("style", "margin-right: 5px");
        btnEditar.setAttribute("data-toggle", "modal");
        btnEditar.setAttribute("data-target", "#formEditProject");
        btnEditar.setAttribute("onclick", "handleShowProjectEditData(" + JSON.stringify(projects[i]) +  ")");
        
        var btnEliminar = document.createElement("button"); btnEliminar.innerText = "Eliminar";
        btnEliminar.setAttribute("class", "btn btn-secondary");
        btnEliminar.setAttribute("onclick", "handleDeleteProject(" + projects[i].id_proyecto + ")");
        tdAcciones.appendChild(btnEditar); tdAcciones.appendChild(btnEliminar);
        
        tr.appendChild(tdAcciones);
                
        tbody.appendChild(tr);
    }
    
    table.appendChild(thead);
    table.appendChild(tbody);
    div.appendChild(table);
}

function handleSaveProject(){
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        saveProject(userData);
    }
}

function saveProject(userData){
    var xhr = new XMLHttpRequest();
    var descripcion = document.getElementById("proyectoInputDescripcion").value;
    var inicio = format(document.getElementById("proyectoInputFechaInicio").value);
    var fin = format(document.getElementById("proyectoInputFechaFin").value);
    var estado = document.getElementById("proyectoInputEstado").value;
    var creador = userData.id_usuario;
    var projectoPost = {
      descripcion: descripcion,
      estado: estado,
      fechaFin: fin,
      fechaInicio: inicio,
      idUsuario: creador
    };
    xhr.open("POST", API_URL + "v1/project/save", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 201){
            var response = JSON.parse(xhr.responseText);
            show("Se ha registrado exitosamente", "success");
            loadProjectView();
        }
        else{
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");            
        }
    };
    xhr.send(JSON.stringify(projectoPost));    
}

function handleDeleteProject(idProject){
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        deleteProject(idProject);
    }    
}

function deleteProject(idProject){
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", API_URL + "v1/project/delete/" + idProject, false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
            show("Se ha eliminado exitosamente", "success");
            loadProjectView();
        }
        else{
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");            
        }
    };
    xhr.send();
}

function format(inputDate) {
    var date = inputDate.split("-");
    if (date !== null && date !== undefined) {
        let stringDate = date[2] + '/' + date[1] + '/' + date[0];
        return stringDate;
    }
}

function handleShowProjectEditData(project){
    document.getElementById("edit_projectInputId").value = project.id_proyecto;
    document.getElementById("edit_proyectoInputFechaInicio").value = project.fecha_inicio;
    document.getElementById("edit_proyectoInputFechaFin").value = project.fecha_fin;
    document.getElementById("edit_proyectoInputDescripcion").value = project.descripcion;
    document.getElementById("edit_proyectoInputEstado").value = project.estado;
}

function handleEditProject(){
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        editProject(userData);
    }
}

function editProject(userData){
    var xhr = new XMLHttpRequest();
    var idProyecto = document.getElementById("edit_projectInputId").value;
    var descripcion = document.getElementById("edit_proyectoInputDescripcion").value;
    var inicio = format(document.getElementById("edit_proyectoInputFechaInicio").value);
    var fin = format(document.getElementById("edit_proyectoInputFechaFin").value);
    var estado = document.getElementById("edit_proyectoInputEstado").value;
    var creador = userData.id_usuario;
    var projectoPost = {
      descripcion: descripcion,
      estado: estado,
      fechaFin: fin,
      fechaInicio: inicio,
      idUsuario: creador,
      idProyecto: idProyecto
    };
    xhr.open("PUT", API_URL + "v1/project/update", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
            var response = JSON.parse(xhr.responseText);
            show("Se ha actualizado exitosamente", "success");
            loadProjectView();
        }
        else{
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");            
        }
    };
    xhr.send(JSON.stringify(projectoPost));    
}