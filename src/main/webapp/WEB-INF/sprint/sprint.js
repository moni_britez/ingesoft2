/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function loadSprintView() {
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        // Si es admin
        if (userData.id_rol == 2) {
            loadUserSprints(userData.id_usuario);
            loadAllSprints();
        } else {
            loadUserSprints(userData.id_usuario);
        }
    }
}

function loadUserSprints(idUsuario) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/sprint/" + idUsuario, false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadSprintsInDOM(response, "misSprintsTable");
            } else {
                var div = document.getElementById("misSprintsTable");
                removeAllChilds(div);
                var messageH6 = document.createElement("h6");
                messageH6.innerText = "No hay nada para mostrar";
                div.appendChild(messageH6);
            }
        } else {
            var div = document.getElementById("misSprintsTable");
            removeAllChilds(div);
            var messageH6 = document.createElement("h6");
            messageH6.innerText = "No hay nada para mostrar";
            div.appendChild(messageH6);
        }
    };
    xhr.send();
}

function loadAllSprints() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/sprint/all", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadSprintsInDOM(response, "allSprintsTable");
            } else {
                var div = document.getElementById("allSprintsTable");
                removeAllChilds(div);
                var messageH6 = document.createElement("h6");
                messageH6.innerText = "No hay nada para mostrar";
                div.appendChild(messageH6);
            }
        } else {
            var div = document.getElementById("allSprintsTable");
            removeAllChilds(div);
            var messageH6 = document.createElement("h6");
            messageH6.innerText = "No hay nada para mostrar";
            div.appendChild(messageH6);
        }
    };
    xhr.send();
}

function loadSprintsInDOM(projects, divTable) {

    var div = document.getElementById(divTable);
    removeAllChilds(div);
    var table = document.createElement("table");

    // creacion de la tabla
    table.setAttribute("class", "table table-hover");
    table.setAttribute("style", "margin-top: 20px");

    // cabeceras

    var thead = document.createElement("thead");
    var trhead = document.createElement("tr");

    var thIdHead = document.createElement("th");
    thIdHead.innerText = "#";
    thIdHead.setAttribute("scope", "col");
    trhead.appendChild(thIdHead);

    var thNombreHead = document.createElement("th");
    thNombreHead.innerText = "Nombre";
    thNombreHead.setAttribute("scope", "col");
    trhead.appendChild(thNombreHead);

    var thDescriptionHead = document.createElement("th");
    thDescriptionHead.innerText = "Descripcion";
    thDescriptionHead.setAttribute("scope", "col");
    trhead.appendChild(thDescriptionHead);

    var thProyectoHead = document.createElement("th");
    thProyectoHead.innerText = "Proyecto";
    thProyectoHead.setAttribute("scope", "col");
    trhead.appendChild(thProyectoHead);

    var thCreadorHead = document.createElement("th");
    thCreadorHead.innerText = "Creador";
    thCreadorHead.setAttribute("scope", "col");
    trhead.appendChild(thCreadorHead);

    var thEstadoHead = document.createElement("th");
    thEstadoHead.innerText = "Estado";
    thEstadoHead.setAttribute("scope", "col");
    trhead.appendChild(thEstadoHead);

    var thDuracion = document.createElement("th");
    thDuracion.innerText = "Duracion";
    thDuracion.setAttribute("scope", "col");
    trhead.appendChild(thDuracion);

    var thActionsHead = document.createElement("th");
    thActionsHead.innerText = "Acciones";
    thActionsHead.setAttribute("scope", "col");
    trhead.appendChild(thActionsHead);

    thead.appendChild(trhead);

    var tbody = document.createElement("tbody");

    for (var i = 0; i < projects.length; i++) {

        var tr = document.createElement("tr");

        var tdId = document.createElement("td");
        tdId.innerText = projects[i].idSprint;
        tr.appendChild(tdId);

        var tdNombre = document.createElement("td");
        tdNombre.innerText = projects[i].nombre;
        tr.appendChild(tdNombre);

        var tdDescripcion = document.createElement("td");
        tdDescripcion.innerText = projects[i].descripcion;
        tr.appendChild(tdDescripcion);

        var tdProyecto = document.createElement("td");
        tdProyecto.innerText = projects[i].idProyecto.descripcion;
        tr.appendChild(tdProyecto);

        var tdCreador = document.createElement("td");
        tdCreador.innerText = projects[i].idUsuario.nombre + " " + projects[i].idUsuario.apellido;
        tr.appendChild(tdCreador);

        var tdEstado = document.createElement("td");
        tdEstado.innerText = projects[i].estado;
        tr.appendChild(tdEstado);

        var tdDuracion = document.createElement("td");
        tdDuracion.innerText = projects[i].duracion;
        tr.appendChild(tdDuracion);

        var tdAcciones = document.createElement("td");
        var btnEditar = document.createElement("button");
        btnEditar.innerText = "Editar";
        btnEditar.setAttribute("class", "btn btn-primary");
        btnEditar.setAttribute("style", "margin-right: 5px");
        btnEditar.setAttribute("data-toggle", "modal");
        btnEditar.setAttribute("data-target", "#formEditSprint");
        btnEditar.setAttribute("onclick", "handleShowSprintEditData(" + JSON.stringify(projects[i]) +  ")");
        
        var btnEliminar = document.createElement("button");
        btnEliminar.innerText = "Eliminar";
        btnEliminar.setAttribute("class", "btn btn-secondary");
        btnEliminar.setAttribute("onclick", "handleDeleteSprint(" + projects[i].idSprint + ")");        
        
        tdAcciones.appendChild(btnEditar);
        tdAcciones.appendChild(btnEliminar);

        tr.appendChild(tdAcciones);

        tbody.appendChild(tr);
    }

    table.appendChild(thead);
    table.appendChild(tbody);
    div.appendChild(table);
}

function loadProjectsInSelect() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/project/all", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadProjectsInSelectDOM(response);
            } else {
                var div = document.getElementById("sprintInputProyecto");
                removeAllChilds(div);
                var select = document.createElement("select");
                select.setAttribute("class", "form-control");
                var option = document.createElement("option");
                option.setAttribute("value", "-1");
                option.innerText = "No hay proyectos";
                select.appendChild(option);
                div.appendChild(select);
            }
        } else {
            var div = document.getElementById("sprintInputProyecto");
            removeAllChilds(div);
            var select = document.createElement("select");
            select.setAttribute("class", "form-control");
            select.setAttribute("id", "sprintSelectInputProyecto");
            var option = document.createElement("option");
            option.setAttribute("value", "-1");
            option.innerText = "No hay proyectos";
            select.appendChild(option);
            div.appendChild(select);
        }
    };
    xhr.send();
}

function loadProjectsInSelectDOM(projects) {
    var div = document.getElementById("sprintInputProyecto");
    removeAllChilds(div);
    var select = document.createElement("select");
    select.setAttribute("class", "form-control");
    select.setAttribute("id", "sprintSelectInputProyecto");
    for (var i = 0; i < projects.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", projects[i].id_proyecto);
        option.innerText = projects[i].descripcion;
        select.appendChild(option);
    }
    div.appendChild(select);
}

function handleSaveSprint() {
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        saveSprint(userData);
    }
}

function saveSprint(userData) {
    var xhr = new XMLHttpRequest();
    var descripcion = document.getElementById("sprintInputDescripcion").value;
    var duracion = document.getElementById("sprintInputDuracion").value;
    var estado = document.getElementById("sprintInputEstado").value;
    var idProyecto = document.getElementById("sprintSelectInputProyecto").value;
    var idUsuario = userData.id_usuario;
    var nombre = document.getElementById("sprintInputNombre").value;
    var sprintPost = {
        descripcion: descripcion,
        duracion: duracion,
        estado: estado,
        idProyecto: idProyecto,
        idUsuario: idUsuario,
        nombre: nombre
    };
    xhr.open("POST", API_URL + "v1/sprint/save", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
            var response = JSON.parse(xhr.responseText);
            show("Se ha registrado exitosamente", "success");
            loadSprintView();
        } else {
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");
        }
    };
    xhr.send(JSON.stringify(sprintPost));
}

function loadSelectsBacklog(){
    loadSprintInSelect();
    loadUsersInSelect();
}

function loadSprintInSelect() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/sprint/all", false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadSprintInDOM(response);
            } else {
                var div = document.getElementById("backlogInputSprint");
                removeAllChilds(div);
                var select = document.createElement("select");
                select.setAttribute("class", "form-control");
                var option = document.createElement("option");
                option.setAttribute("value", "-1");
                option.innerText = "No hay sprints";
                select.appendChild(option);
                div.appendChild(select);
            }
        } else {
            var div = document.getElementById("backlogInputSprint");
            removeAllChilds(div);
            var select = document.createElement("select");
            select.setAttribute("class", "form-control");
            select.setAttribute("id", "selectBacklogInputSprint");
            var option = document.createElement("option");
            option.setAttribute("value", "-1");
            option.innerText = "No hay sprints";
            select.appendChild(option);
            div.appendChild(select);
        }
    };
    xhr.send();
}

function loadSprintInDOM(projects) {
    var div = document.getElementById("backlogInputSprint");
    removeAllChilds(div);
    var select = document.createElement("select");
    select.setAttribute("class", "form-control");
    select.setAttribute("id", "selectBacklogInputSprint");
    for (var i = 0; i < projects.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", projects[i].idSprint);
        option.innerText = projects[i].nombre;
        select.appendChild(option);
    }
    div.appendChild(select);
}

function loadUsersInSelect() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/user/all", false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadUsersInDOM(response);
            } else {
                var div = document.getElementById("backlogInputEncargado");
                removeAllChilds(div);
                var select = document.createElement("select");
                select.setAttribute("class", "form-control");
                var option = document.createElement("option");
                option.setAttribute("value", "-1");
                option.innerText = "No hay usuarios";
                select.appendChild(option);
                div.appendChild(select);
            }
        } else {
            var div = document.getElementById("backlogInputEncargado");
            removeAllChilds(div);
            var select = document.createElement("select");
            select.setAttribute("class", "form-control");
            select.setAttribute("id", "selectBacklogInputEncargado");
            var option = document.createElement("option");
            option.setAttribute("value", "-1");
            option.innerText = "No hay usuarios";
            select.appendChild(option);
            div.appendChild(select);
        }
    };
    xhr.send();
}

function loadUsersInDOM(projects) {
    var div = document.getElementById("backlogInputEncargado");
    removeAllChilds(div);
    var select = document.createElement("select");
    select.setAttribute("class", "form-control");
    select.setAttribute("id", "selectBacklogInputEncargado");
    for (var i = 0; i < projects.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", projects[i].id_usuario);
        option.innerText = projects[i].nombre + " " + projects[i].apellido;
        select.appendChild(option);
    }
    div.appendChild(select);
}

function handleShowSprintEditData(sprint){
    document.getElementById("edit_sprintInputId").value = sprint.idSprint;
    document.getElementById("edit_sprintInputNombre").value = sprint.nombre;
    document.getElementById("edit_sprintInputDescripcion").value = sprint.descripcion;
    document.getElementById("edit_sprintInputEstado").value = sprint.estado;
    document.getElementById("edit_sprintInputDuracion").value = sprint.duracion;
    loadProjectsEditInSelect(sprint.idProyecto.idProyecto);    
}

function loadProjectsEditInSelect(idProyecto) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/project/all", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadProjectsEditInSelectDOM(response, idProyecto);
            } else {
                var div = document.getElementById("edit_sprintInputProyecto");
                removeAllChilds(div);
                var select = document.createElement("select");
                select.setAttribute("class", "form-control");
                var option = document.createElement("option");
                option.setAttribute("value", "-1");
                option.innerText = "No hay proyectos";
                select.appendChild(option);
                div.appendChild(select);
            }
        } else {
            var div = document.getElementById("edit_sprintInputProyecto");
            removeAllChilds(div);
            var select = document.createElement("select");
            select.setAttribute("class", "form-control");
            select.setAttribute("id", "editselect_sprintInputProyecto");
            var option = document.createElement("option");
            option.setAttribute("value", "-1");
            option.innerText = "No hay proyectos";
            select.appendChild(option);
            div.appendChild(select);
        }
    };
    xhr.send();
}

function loadProjectsEditInSelectDOM(projects, idProyecto) {
    var div = document.getElementById("edit_sprintInputProyecto");
    removeAllChilds(div);
    var select = document.createElement("select");
    select.setAttribute("class", "form-control");
    select.setAttribute("id", "editselect_sprintInputProyecto");
    for (var i = 0; i < projects.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", projects[i].id_proyecto);
        option.innerText = projects[i].descripcion;
        if(projects[i].id_proyecto == idProyecto){
            option.setAttribute("selected", "true");
        }
        select.appendChild(option);
    }
    div.appendChild(select);
}

function handleEditSprint() {
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        editSprint(userData);
    }
}

function editSprint(userData) {
    var xhr = new XMLHttpRequest();
    var descripcion = document.getElementById("edit_sprintInputDescripcion").value;
    var duracion = document.getElementById("edit_sprintInputDuracion").value;
    var estado = document.getElementById("edit_sprintInputEstado").value;
    var idProyecto = document.getElementById("editselect_sprintInputProyecto").value;
    var idUsuario = userData.id_usuario;
    var idSprint = document.getElementById("edit_sprintInputId").value;
    var nombre = document.getElementById("edit_sprintInputNombre").value;
    var sprintPost = {
        descripcion: descripcion,
        duracion: duracion,
        estado: estado,
        idProyecto: idProyecto,
        idUsuario: idUsuario,
        idSprint: idSprint,
        nombre: nombre
    };
    xhr.open("PUT", API_URL + "v1/sprint/update", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            show("Se ha actualizado exitosamente", "success");
            loadSprintView();
        } else {
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");
        }
    };
    xhr.send(JSON.stringify(sprintPost));
}

function handleDeleteSprint(idProject){
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        deleteSprint(idProject);
    }    
}

function deleteSprint(idProject){
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", API_URL + "v1/sprint/delete/" + idProject, false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
            show("Se ha eliminado exitosamente", "success");
            loadSprintView();
        }
        else{
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");            
        }
    };
    xhr.send();
}