<div class="modal fade" id="formSprint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo sprint</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" id="sprintInputNombre" placeholder="Nombre">
                    </div>                    
                    <div class="form-group">
                        <input type="text" class="form-control" id="sprintInputDescripcion" placeholder="Descripcion">
                    </div>
                    <div class="form-group">
                        <label for="sprintInputProyecto">Proyecto</label>                     
                        <div id="sprintInputProyecto"></div>
                    </div>
                    <div class="form-group">
                        <label for="sprintInputEstado">Estado</label>                                                
                        <input type="text" class="form-control" id="sprintInputEstado" placeholder="Estado" value="PENDIENTE">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="sprintInputDuracion" placeholder="Duracion">
                    </div>                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="handleSaveSprint()">Guardar</button>                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>