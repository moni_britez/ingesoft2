<div class="modal fade" id="formEditBacklog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar tarea</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="text" id="edit_backlogInputId" hidden="true"/>
                        <label for="edit_backlogInputDescripcion" >Descripcion</label>
                        <input type="text" class="form-control" id="edit_backlogInputDescripcion" placeholder="Descripcion">
                    </div>                    
                    <div class="form-group">
                        <label for="edit_backlogInputPrioridad">Prioridad</label>                        
                        <input type="text" class="form-control" id="edit_backlogInputPrioridad" placeholder="Prioridad">
                    </div>
                    <div class="form-group">
                        <label for="edit_backlogInputDuracion">Duracion</label> 
                        <input type="number" class="form-control" id="edit_backlogInputDuracion" placeholder="Duracion">
                    </div>
                    <div class="form-group">
                        <label for="edit_backlogInputSprint">Sprint</label>
                        <div id="edit_backlogInputSprint"></div>
                    </div>
                    <div class="form-group">
                        <label for="edit_backlogInputEncargado">Encargado</label>
                        <div id="edit_backlogInputEncargado"></div>
                    </div>
                </form>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="handleEditBacklog()">Guardar</button>                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>