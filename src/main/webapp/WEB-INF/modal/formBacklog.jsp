<div class="modal fade" id="formBacklog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nueva tarea</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" id="backlogInputDescripcion" placeholder="Descripcion">
                    </div>                    
                    <div class="form-group">
                        <input type="text" class="form-control" id="backlogInputPrioridad" placeholder="Prioridad">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="backlogInputDuracion" placeholder="Duracion">
                    </div>
                    <div class="form-group">
                        <label for="backlogInputSprint">Sprint</label>
                        <div id="backlogInputSprint"></div>
                    </div>
                    <div class="form-group">
                        <label for="backlogInputEncargado">Encargado</label>
                        <div id="backlogInputEncargado"></div>
                    </div>
                </form>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="handleSaveBacklog()">Guardar</button>                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>