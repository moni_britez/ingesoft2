<div class="modal fade" id="formProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo proyecto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="proyectoInputDescripcion">Descripcion</label>
                        <input type="text" class="form-control" id="proyectoInputDescripcion" placeholder="Descripcion">
                    </div>
                    <div class="form-group">
                        <label for="proyectoInputFechaInicio">Inicio</label>                        
                        <input type="date" class="form-control" id="proyectoInputFechaInicio" placeholder="Fecha inicio">
                    </div>
                    <div class="form-group">
                        <label for="proyectoInputFechaFin">Fin</label>                                                
                        <input type="date" class="form-control" id="proyectoInputFechaFin" placeholder="Fecha fin">
                    </div>
                    <div class="form-group">
                        <label for="proyectoInputEstado">Estado</label>                                                
                        <input type="text" class="form-control" id="proyectoInputEstado" placeholder="Estado" value="PENDIENTE">
                    </div>                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="handleSaveProject()" data-dismiss="modal">Guardar</button>                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>