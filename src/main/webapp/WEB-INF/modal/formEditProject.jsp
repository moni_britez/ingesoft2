<div class="modal fade" id="formEditProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar proyecto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="text" id="edit_projectInputId" hidden="true"/>
                        <label for="edit_proyectoInputDescripcion">Descripcion</label>
                        <input type="text" class="form-control" id="edit_proyectoInputDescripcion" placeholder="Descripcion">
                    </div>
                    <div class="form-group">
                        <label for="edit_proyectoInputFechaInicio">Inicio</label>                        
                        <input type="date" class="form-control" id="edit_proyectoInputFechaInicio" placeholder="Fecha inicio">
                    </div>
                    <div class="form-group">
                        <label for="edit_proyectoInputFechaFin">Fin</label>                                                
                        <input type="date" class="form-control" id="edit_proyectoInputFechaFin" placeholder="Fecha fin">
                    </div>
                    <div class="form-group">
                        <label for="edit_proyectoInputEstado">Estado</label>                                                
                        <input type="text" class="form-control" id="edit_proyectoInputEstado" placeholder="Estado">
                    </div>                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="handleEditProject()" data-dismiss="modal">Guardar</button>                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>