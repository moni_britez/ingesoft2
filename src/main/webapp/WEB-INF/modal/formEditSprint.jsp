<div class="modal fade" id="formEditSprint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar sprint</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="text" hidden id="edit_sprintInputId"/>
                    <div class="form-group">
                        <label for="edit_sprintInputNombre">Descripcion</label>
                        <input type="text" class="form-control" id="edit_sprintInputNombre" placeholder="Nombre">
                    </div>                    
                    <div class="form-group">
                        <label for="edit_sprintInputDescripcion">Descripcion</label>
                        <input type="text" class="form-control" id="edit_sprintInputDescripcion" placeholder="Descripcion">
                    </div>
                    <div class="form-group">
                        <label for="edit_sprintInputProyecto">Proyecto</label>                     
                        <div id="edit_sprintInputProyecto"></div>
                    </div>
                    <div class="form-group">
                        <label for="edit_sprintInputEstado">Estado</label>                                                
                        <input type="text" class="form-control" id="edit_sprintInputEstado" placeholder="Estado">
                    </div>
                    <div class="form-group">
                        <label for="edit_sprintInputDuracion">Duracion</label>                                                                        
                        <input type="number" class="form-control" id="edit_sprintInputDuracion" placeholder="Duracion">
                    </div>                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="handleEditSprint()">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>