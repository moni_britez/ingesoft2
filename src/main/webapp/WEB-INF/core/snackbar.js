/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function show(message, type){
    var body = document.getElementById("currentBody");
    var snackbar = document.createElement("div");
    var background = type === "success" ? "#43a047" : "#d32f2f";
    var snackbarText = document.createElement("div");
    snackbar.setAttribute("style", "position: fixed; display: flex; z-index: 1; top: 0; width: 100%; justify-content: center");    
    snackbar.setAttribute("id", "snackbar-notification");
    snackbarText.setAttribute("style", "width: auto; color: white; background: " + background + "; padding: 10px; border-radius: 5px");
    snackbarText.innerText = message;
    snackbar.appendChild(snackbarText);
    body.appendChild(snackbar);
    sleep(2000).then(()=>{
        body.removeChild(snackbar);
    });
}

function sleep(time){
    return new Promise((resolve) => setTimeout(resolve, time));
}
