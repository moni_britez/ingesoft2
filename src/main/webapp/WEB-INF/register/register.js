/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function goToLogin(){ window.location.href = "login"; }
function makeRegister(){
    var xhr = new XMLHttpRequest();
    var nombre = document.getElementById("inputNombre").value;
    var apellido = document.getElementById("inputApellido").value;
    var email = document.getElementById("inputEmail").value;
    var username = document.getElementById("inputUsername").value;
    var password = document.getElementById("inputPassword").value;
    var telefono = document.getElementById("inputTelefono").value;
    var rol = document.getElementById("rol-select").value;
    var registerRequest = {
        nombre: nombre,
        apellido: apellido,
        correo: email,
        usuario: username,
        contrasenha: password,
        id_rol: rol,
        telefono: telefono
    };
    xhr.open("POST", API_URL + "v1/user/save", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 201){
            var response = JSON.parse(xhr.responseText);
            show("Se ha registrado exitosamente", "success");
            limpiarCampos();
            setTimeout(function(){ goToLogin(); }, 2000);
        }
        else{
            var response = xhr.responseText;
            show("Ha ocurrido un error al procesar la solicitud", "error");            
        }
    };
    xhr.send(JSON.stringify(registerRequest));    
}
function loadRoles(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/role/all", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function(){
        if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
            var response  = JSON.parse(xhr.responseText);
            loadRolesDOM(response);
        }
        else{
            var response  = JSON.parse(xhr.responseText);
            alert("Servicio no disponible");
            window.location.href = "login";
        }
    };
    xhr.send();
}
function loadRolesDOM(roles){
    var divPadre = document.getElementById("rol-div");
    var select = document.createElement("select");
    select.setAttribute("class", "form-control");
    select.setAttribute("id", "rol-select");
    for(var i = 0; i < roles.length; i++){
        var option = document.createElement("option");
        option.setAttribute("value", roles[i].id);
        option.innerText = roles[i].nombre;
        select.appendChild(option);
    }
    divPadre.appendChild(select);    
}

function limpiarCampos(){
    document.getElementById("inputNombre").value = "";
    document.getElementById("inputApellido").value = "";
    document.getElementById("inputEmail").value = "";
    document.getElementById("inputUsername").value = "";
    document.getElementById("inputPassword").value = "";
    document.getElementById("inputTelefono").value = "";
}