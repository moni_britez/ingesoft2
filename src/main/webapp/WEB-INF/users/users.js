/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function loadUsersView() {
    var userData = readCookie("inge_userdata");
    if (userData === null || userData === undefined) {
        window.location.href = "login";
    } else {
        userData = JSON.parse(userData);
        loadAllUsers();
    }
}

function loadAllUsers() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", API_URL + "v1/user/all", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.length > 0) {
                loadAllUsersDOM(response, "allUsuarios");
            } else {
                var div = document.getElementById("allUsuarios");
                removeAllChilds(div);
                var messageH6 = document.createElement("h6");
                messageH6.innerText = "No hay nada para mostrar";
                div.appendChild(messageH6);
            }
        } else {
            var div = document.getElementById("allUsuarios");
            removeAllChilds(div);
            var messageH6 = document.createElement("h6");
            messageH6.innerText = "No hay nada para mostrar";
            div.appendChild(messageH6);
        }
    };
    xhr.send();
}


function loadAllUsersDOM(projects, divTable) {

    var div = document.getElementById(divTable);
    removeAllChilds(div);
    var table = document.createElement("table");

    // creacion de la tabla
    table.setAttribute("class", "table table-hover");
    table.setAttribute("style", "margin-top: 20px");

    // cabeceras

    var thead = document.createElement("thead");
    var trhead = document.createElement("tr");

    var thIdHead = document.createElement("th");
    thIdHead.innerText = "#";
    thIdHead.setAttribute("scope", "col");
    trhead.appendChild(thIdHead);

    var thNombreHead = document.createElement("th");
    thNombreHead.innerText = "Nombre";
    thNombreHead.setAttribute("scope", "col");
    trhead.appendChild(thNombreHead);

    var thApellidoHead = document.createElement("th");
    thApellidoHead.innerText = "Apellido";
    thApellidoHead.setAttribute("scope", "col");
    trhead.appendChild(thApellidoHead);

    var thUsuarioHead = document.createElement("th");
    thUsuarioHead.innerText = "Usuario";
    thUsuarioHead.setAttribute("scope", "col");
    trhead.appendChild(thUsuarioHead);

    var thCorreoHead = document.createElement("th");
    thCorreoHead.innerText = "Correo";
    thCorreoHead.setAttribute("scope", "col");
    trhead.appendChild(thCorreoHead);

    var thRol = document.createElement("th");
    thRol.innerText = "Rol";
    thRol.setAttribute("scope", "col");
    trhead.appendChild(thRol);

    thead.appendChild(trhead);

    var tbody = document.createElement("tbody");

    for (var i = 0; i < projects.length; i++) {

        var tr = document.createElement("tr");

        var tdId = document.createElement("td");
        tdId.innerText = projects[i].id_usuario;
        tr.appendChild(tdId);

        var tdNombre = document.createElement("td");
        tdNombre.innerText = projects[i].nombre;
        tr.appendChild(tdNombre);

        var tdApellido = document.createElement("td");
        tdApellido.innerText = projects[i].apellido;
        tr.appendChild(tdApellido);

        var tdUsuario = document.createElement("td");
        tdUsuario.innerText = projects[i].usuario;
        tr.appendChild(tdUsuario);

        var tdCorreo = document.createElement("td");
        tdCorreo.innerText = projects[i].correo;
        tr.appendChild(tdCorreo);

        var tdRol = document.createElement("td");
        tdRol.innerText = projects[i].rol.nombre;
        tr.appendChild(tdRol);

        tbody.appendChild(tr);
    }

    table.appendChild(thead);
    table.appendChild(tbody);
    div.appendChild(table);
}