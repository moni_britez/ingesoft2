/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function loadUserData(){
    var div = document.getElementById("datosPersonales");
    removeAllChilds(div);    
    var data = readCookie("inge_userdata");
    if (data === null || data === undefined) {
        window.location.href = "login";
    } else {
        data = JSON.parse(data);
        var nombreH6 = document.createElement("h6");
        var correoH6 = document.createElement("h6");
        nombreH6.innerHTML = "<strong>Nombre y apellido: </strong>" + data.nombre + " " + data.apellido;
        correoH6.innerHTML = "<strong>Correo: </strong>" + data.correo;
        div.appendChild(nombreH6);
        div.appendChild(correoH6);
    }
}