<%-- 
    Document   : login
    Created on : 25/06/2019, 10:41:07 PM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proyectos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="resources/bootstrap.min.css">
        <script src="resources/bootstrap.min.js"></script>
        <script><%@include file="/WEB-INF/configuration/config.js"%></script>        
    </head>
    <body onload="loadUserData()">
        <div>
            <h3>Acerca de</h3>
            <h5 style="margin-top: 20px">Datos personales</h5>            
            <div id="datosPersonales">
            </div>
            <h5 style="margin-top: 20px">Integrantes</h5>
            <ul>
                <li>Florencia Franco</li>
                <li>Monica Britez</li>
                <li>Danilo Reyes</li>
                <li>Roman Veron</li>
            </ul>
        </div>
    </body>
</html>
